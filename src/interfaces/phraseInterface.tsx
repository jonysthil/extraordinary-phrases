// Generated by https://quicktype.io

export interface PhraseDBNow {
    fra_act:   boolean;
    fra_id:    number;
    fra_name:  string;
    fra_autor: string;
}
