import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native';

interface Props {
    status: string;
    fecha: string;
}

export const HomeDetail = ({ status, fecha }: Props) => {

    const navigation = useNavigation();

    const [currentDate, setCurrentDate] = useState('');

    const tomaFecha = async () => {
        var date = new Date().getDate();
		var month = new Date().getMonth() + 1;
		var year = new Date().getFullYear();
		setCurrentDate(date + '/' + month + '/' + year);
    }

	useEffect(() => {
		tomaFecha();
	}, []);

    const getMyStringValue = async () => {
		try {
		  	await AsyncStorage.setItem('status', 'on');
			await AsyncStorage.setItem('datePhrase', currentDate);
            navigation.navigate('Phrase');
		} catch(e) {
			console.log(e);
		}
	  
	}

    return (
        <>
            <View style={ styles.description }>
					<Text style={ styles.title }>
						Obten tú frase para empezar tu día con alegría y felicidad.
					</Text>
				</View>
				<TouchableOpacity 
					style={styles.button} 
					onPress={() => getMyStringValue()} 
				>
					<Text style={ styles.buttonTitle }>Obtener</Text>
				</TouchableOpacity>
        </>
    )
}

const styles = StyleSheet.create({
	description: {
		alignContent: 'center',
		alignItems: 'center',
		marginBottom: 15,
		padding: 10,
		marginTop: -30
	},
	title: {
		fontSize:35,
		color: '#fff',
		textAlign: 'center'
	},
	button: {
		alignItems: 'center',
		justifyContent: 'center',
		paddingVertical: 12,
		paddingHorizontal: 32,
		borderRadius: 4,
		elevation: 3,
		backgroundColor: '#d95582',
	},
	buttonTitle: {
		fontSize: 16,
		lineHeight: 21,
		fontWeight: 'bold',
		letterSpacing: 0.25,
		color: 'white',
	}
});
