import React from 'react'
import { StyleSheet, StatusBar, SafeAreaView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

interface Props {
    children: JSX.Element | JSX.Element[]
}

export const GradintBackgraound = ({children}: Props) => {

    return (
        <SafeAreaView style={{ flex: 1 }}>

            <StatusBar
                animated= {true}
                backgroundColor= '#440078'
                barStyle= 'light-content'
                showHideTransition= 'fade'
            />

            <LinearGradient 
                colors={['#440078', '#6c0076']} 
                start={{x: 0.0, y: 0.25}} 
                end={{x: 0.5, y: 1.0}}
                style={{ ...StyleSheet.absoluteFillObject }}
            />

            {children}

        </SafeAreaView>
    )
}
 