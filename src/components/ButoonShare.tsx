import React from 'react';
import { StyleSheet, Share, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

interface Props {
    phrase?: string;
    author?: string;
}

export const ButoonShare = ({phrase, author}: Props) => {

    const onShare = async () => {
        try {
            const result = await Share.share({
                 message: `${author} | ${phrase}`,
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    console.log('cerrando 0');
                } else {
                    console.log('cerrando 1');
                }
            } else if (result.action === Share.dismissedAction) {
              console.log('cerrando');
            }
          } catch (error) {
            console.log(error.message);
          }
    }

    return (
        <>
            <View style={styles.containter}>
                <TouchableOpacity onPress={onShare}>
                    <Icon
                        name='share-social-outline'
                        size={30}
                        color='#ffffff'
                    />
                </TouchableOpacity>
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    containter: {
        position: 'absolute',
        left: 15,
        bottom: 0,
    }
})
