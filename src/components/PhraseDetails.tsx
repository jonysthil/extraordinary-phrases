import { StackActions, useNavigation } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { PhraseDBNow } from '../interfaces/phraseInterface';
import { ButoonShare } from './ButoonShare';

interface Props {
    phraseFull: PhraseDBNow
}

export const PhraseDetails = ({ phraseFull }: Props) => {

	const reloadPhrase = () => {
		//console.log('hola');
		navigation.dispatch(StackActions.replace('Phrase'))
	}

	const navigation = useNavigation();
	
    return (
        <>
            <View>
                <View style={ styles.description }>
					<Text style={ styles.title }>
                        "{ phraseFull.fra_name }"
					</Text>
				</View>

                <View style={ styles.descriptionAuthor }>
					<ButoonShare phrase={phraseFull.fra_name} author={phraseFull.fra_autor} />
                    <Text style={ styles.author }>-- { phraseFull.fra_autor }</Text>
                </View>

				<TouchableOpacity 
					style={styles.button} 
					onPress={() => reloadPhrase()} 
					 >
					<Text style={ styles.buttonTitle }>Quiero volver a intentarlo</Text>
				</TouchableOpacity>


            </View>
        </>
    )
}

const styles = StyleSheet.create({
	container:{
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'transparent'
	},
	description: {
		alignContent: 'center',
		alignItems: 'center',
		marginBottom: 15,
		padding: 10,
		marginTop: -30
	},
	title: {
		fontSize:30,
		color: '#fff',
		textAlign: 'center'
	},
    descriptionAuthor: {
		padding: 10,
		marginTop: 10,
    },
    author: {
        fontSize: 15,
		color: '#fff',
		textAlign: 'right'
    },
	button: {
		alignItems: 'center',
		justifyContent: 'center',
		paddingVertical: 12,
		paddingHorizontal: 32,
		borderRadius: 4,
		elevation: 3,
		backgroundColor: '#d95582',
		marginRight: 35,
		marginLeft: 35,
		marginTop:25
	},
	buttonTitle: {
		fontSize: 16,
		lineHeight: 21,
		fontWeight: 'bold',
		letterSpacing: 0.25,
		color: 'white',
	}
});
