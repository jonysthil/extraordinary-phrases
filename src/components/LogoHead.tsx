import React from 'react'
import { Image, View, StyleSheet } from 'react-native';

export const LogoHead = () => {

    const uri = 'https://tribuextraordinaria.com/assets/img/tribu/logo-app.png';
    
    return (
        <>
            <View style={ styles.logoContainer }>
                <Image style={styles.logo} source={{ uri }}/>
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    logo: {
        height:100,
		width:300,
		resizeMode: 'center',
		marginBottom: 55
    },
    logoContainer: {
		alignItems:'center',
		alignContent: 'center',
    }
});