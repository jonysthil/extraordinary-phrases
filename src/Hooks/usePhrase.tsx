import { useEffect, useState } from 'react';
import phraseDB from '../api/phraseDB';
import { PhraseDBNow } from '../interfaces/phraseInterface';

interface PhraseDetails {
    isLoading: boolean;
    phraseNow?: PhraseDBNow;
}

export const usePhrase = () => {

    const [state, setstate] = useState<PhraseDetails>({
        isLoading: true,
        phraseNow: undefined
    });

    const getPhraseDetais = async() => {

        const phraseDetailsPromise = phraseDB.get<PhraseDBNow>('/phrase');

        const [phraseDetailsResp] = await Promise.all([phraseDetailsPromise]);

        setstate({
            isLoading: false,
            phraseNow: phraseDetailsResp.data
        })
    }

    useEffect(() => {
        getPhraseDetais();
    }, []);

    return {
        ...state
    }
}
