import React, { useEffect, useState } from 'react';

interface Estatus {
    isValido: boolean
}

export const useStatus = () => {

    const [valida, setValida] = useState<Estatus>({
        isValido: true
    });

    useEffect(() => {
        setValida({
            isValido: false
        })
    }, []);

    return (
        valida
    )
}
