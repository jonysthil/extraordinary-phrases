import React, { useState } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { HomeScreen } from '../screens/HomeScreen';
import { PhraseScreen } from '../screens/PhraseScreen';

export type RootStackParams = {
	Home: undefined;
    Phrase: undefined;
}

const Stack = createStackNavigator<RootStackParams>();


export const Navigation = () => {

	return (
		<Stack.Navigator 
			screenOptions={{
				headerShown: false,
			}}
		>
			<Stack.Screen name="Home" component={HomeScreen} />
			<Stack.Screen name="Phrase" component={PhraseScreen} />
		</Stack.Navigator>
	);
}