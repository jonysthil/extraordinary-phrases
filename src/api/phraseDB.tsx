import axios from 'axios';

const phraseDB = axios.create({
    baseURL: 'https://tribuextraordinaria.com/miembros'
});

export default phraseDB;