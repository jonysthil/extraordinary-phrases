import { useNavigation } from '@react-navigation/core';
import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { GradintBackgraound } from '../components/GradintBackgraound';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { LogoHead } from '../components/LogoHead';

export const HomeScreen = () => {

	const {top} = useSafeAreaInsets();

	const navigation = useNavigation();
	
	return (

		<GradintBackgraound>
			<View style={ [styles.container, {marginTop: top - 180}] }>

				<LogoHead />

				<View style={ styles.description }>
					<Text style={ styles.title }>
						Obten tú frase para empezar tu día con alegría y felicidad.
					</Text>
				</View>
				<TouchableOpacity 
					style={styles.button} 
					onPress={() => navigation.navigate('Phrase')} 
					 >
					<Text style={ styles.buttonTitle }>Obtener</Text>
				</TouchableOpacity>
			</View>
		</GradintBackgraound>
  	)
}

const styles = StyleSheet.create({
	container:{
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'transparent'
	},
	description: {
		alignContent: 'center',
		alignItems: 'center',
		marginBottom: 15,
		padding: 10,
		marginTop: -30
	},
	title: {
		fontSize:35,
		color: '#fff',
		textAlign: 'center'
	},
	button: {
		alignItems: 'center',
		justifyContent: 'center',
		paddingVertical: 12,
		paddingHorizontal: 32,
		borderRadius: 4,
		elevation: 3,
		backgroundColor: '#d95582',
	},
	buttonTitle: {
		fontSize: 16,
		lineHeight: 21,
		fontWeight: 'bold',
		letterSpacing: 0.25,
		color: 'white',
	}
});
