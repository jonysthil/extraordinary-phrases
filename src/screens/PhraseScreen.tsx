import React from 'react'
import { ActivityIndicator, Image, StyleSheet, Text, View } from 'react-native';
import { GradintBackgraound } from '../components/GradintBackgraound';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { usePhrase } from '../Hooks/usePhrase';
import { PhraseDetails } from '../components/PhraseDetails';
import { LogoHead } from '../components/LogoHead';


export const PhraseScreen = () => {
	
	const {top} = useSafeAreaInsets();

	const { isLoading, phraseNow } = usePhrase();
	
	return (
		<GradintBackgraound>
			<View style={ [styles.container, {marginTop: top - 180}] }>

				<LogoHead />

				
				{
                	isLoading
                ?

				<ActivityIndicator
					size={35}
					color="grey"
					style={{marginTop: 20}}
				/>
					
                :
                	<PhraseDetails phraseFull={phraseNow!}  />
            	}

				
			</View>
		</GradintBackgraound>
  	)
}

const styles = StyleSheet.create({
	container:{
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'transparent'
	}
});
