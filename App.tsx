import 'react-native-gesture-handler';
import SplashScreen from 'react-native-splash-screen'
import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { Navigation } from './src/navigation/Navigation';

import messaging from '@react-native-firebase/messaging';

const App = () => {

	useEffect(() => SplashScreen.hide());

	useEffect(() => {

		const foregroundSubscriber = messaging().onMessage(async remoteMessage => {
		  console.log('Push notification recibida', JSON.stringify(remoteMessage));
		});

		{/*const topicSubscriber = messaging()
			.subscribeToTopic('jonysthil')
		.then(() => console.log('Subscrito al topico Frases EXTRAordinarias!'));*/}

		const backgroundSubscriber = messaging().setBackgroundMessageHandler(
			async(remoteMessage) => {
				console.log('Push notification en background', remoteMessage);
			}
		);
	  
	
		return () => {
			foregroundSubscriber;
			//topicSubscriber;
		};
	  }, []);
	
	return (
		<NavigationContainer>
			<Navigation />
		</NavigationContainer>
	)
}

export default App;
